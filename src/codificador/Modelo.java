package codificador;

//Importo las librerías de AWT y de SQL
import java.awt.List;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Modelo {

    //Declaro los atributos String que utilizaré para conectarme query la base de datos
    private String driver;
    private String prefijoConexion;
    private String ip;
    private String usr;
    private String psw;
    private String bd;

    //Declaro un string 'tabla' que utilizaré para consultar query la base de datos
    private String tabla;

    //Declaro un atributo de tipo Connection, que representará mi referencia query la base de datos
    private Connection connection;
    //Declaro un atributo de tipo ActionListener, que representa el evento que se activará 
    //cuando ocurra una excepción
    private ActionListener listener;
    private final ArrayList<Character> numeros;
    private final ArrayList<Character> imprimibles;
    private String resultadoCodificado;

//Declaro un constructor vacío del modelo, donde inicializo algunos de sus atributos de tipo String
    public Modelo() {
        this.imprimibles = new ArrayList<Character>(Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '.', ';', ',', ':', '!', '?'));
        this.numeros = new ArrayList<Character>(Arrays.asList('0', '1', '2', '3', '4', '5'));
        driver = "com.mysql.jdbc.Driver";
        prefijoConexion = "jdbc:mysql://";
        ip = "localhost";             // Direccion IP donde esta corriendo el SGBD
        usr = "";                     // Usuario
        psw = "";                     // Password
        bd = "codigo";               // Base de datos
        tabla = "representacion";         // Tabla
        resultadoCodificado = "";
    }

    private String decodificarCaracter(Character car) throws SQLException {
        String traduccion;
        String decod = "";

        Statement statement = connection.createStatement();
        ResultSet resultSet;
        String query;
        if (car == '\n') {
            return "\n";
        }
        if (imprimibles.contains(car)) {
            query = "SELECT traduccion FROM " + tabla + " WHERE caracter='" + car + "'";

            resultSet = statement.executeQuery(query);

            resultSet.next();
            traduccion = resultSet.getString(1);

            for (Character c : traduccion.toCharArray()) {
                if (numeros.contains(c)) {
                    query = "SELECT traduccion FROM " + tabla + " WHERE caracter='" + c + "'";

                    resultSet = statement.executeQuery(query);
                    resultSet.next();
                    decod += resultSet.getString(1);

                }
            }
            resultSet.close();
            statement.close();
            return decod;
        }

        return null;
    }

    public String getResultadoCodificado() {
        return this.resultadoCodificado;
    }

    public boolean codificarTexto(String texto) throws SQLException {
        connection = obtenerConexion();
        String textoCodificado = "";
        boolean error = false;
        for (Character c : texto.toCharArray()) {
            if (decodificarCaracter(c) == null) {
                return false;
            } else if (decodificarCaracter(c) == "\n") {
                textoCodificado += "\n";
            } else {
                textoCodificado += decodificarCaracter(c) + " ";
            }
        }
        this.resultadoCodificado = textoCodificado;
        return true;
    }

    //El modelo recibe una referencia al evento que se activará cuando ocurra una excepción
    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    //Este método se ejecutará cuando ocurra una excepción
    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    //Codifico el método 'obtenerConexion', que me devolverá una referencia query la base de datos
    //a la cual quiero acceder.
    private Connection obtenerConexion() {
        if (connection == null) {
            try {
                //Registra el driver en la máquina virtual
                Class.forName(driver);
            } catch (ClassNotFoundException ex) {
                reportException(ex.getMessage());
            }
            try {
                //Le paso la dirección de la base de datos, un nombre de usuario, una contraseña
                //y me devuelve una referencia a la base de datos
                String s = this.prefijoConexion + this.ip + "/" + this.bd;
                connection = DriverManager.getConnection(s, usr, psw);
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
            Runtime.getRuntime().addShutdownHook(new ShutDownHook());
        }
        //Devuelvo la referencia a la base de datos
        return connection;
    }

    //Cierro la conexión a la base de datos
    private class ShutDownHook extends Thread {

        @Override
        public void run() {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
    }
}
