package codificador;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Vista {

    private Modelo modelo;
    private JFrame frame;
    private JPanel panel;

    private JTextArea entrada;
    private JTextArea salida;
    private JScrollPane scrollEntrada;
    private JScrollPane scrollSalida;
    private JTextField textFieldPadre;
    
    private JButton botonCodificar;
    
    //Al constructor de la vista le paso una referencia al modelo
    public Vista(Modelo modelo) {
        this.modelo = modelo;
        //Le paso un exceptionListener al modelo
        this.modelo.addExceptionListener(new ExceptionListener());
        
        //En el constructor, hago el maquetado de la vista
        
        //Instancio los componentes de la vista
        frame = new JFrame();
        panel = new JPanel();

        panel.setLayout(new FlowLayout());
        entrada = new JTextArea();
        entrada.setColumns(40);
        entrada.setRows(10);
        entrada.setLineWrap(true);
       entrada.setWrapStyleWord(true);
        entrada.setFont(new Font("Monospaced",Font.PLAIN,16));
        
        
        salida = new JTextArea();
        salida.setColumns(60);
        salida.setRows(10);
        salida.setLineWrap(true);
        salida.setWrapStyleWord(true);
        salida.setFont(new Font("Monospaced",Font.PLAIN,16));

        botonCodificar = new JButton("Codificar");

        //Formateo el frame
        frame.getContentPane().setLayout(new FlowLayout()); 
        frame.getContentPane().setBackground(Color.GRAY);
        
        scrollEntrada=new JScrollPane(entrada);
        scrollSalida=new JScrollPane(salida);

        //Agrego al panel los scrolls y el botón
        
  
        panel.add(scrollEntrada);
        panel.add(botonCodificar);
        panel.add(scrollSalida);
        

        //Agrego el panel al frame
        frame.getContentPane().add(panel);
    }

    //Muestra el frame cargado con lo que instancié y agregué en el constructor
    public void mostrar() {
        //Pongo el título al frame
        frame.setTitle("Codificador");
        //Le digo al frame que cuando se cierre la ventana, salga del programa
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Formateo el tamaño del frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        //Hago visible el frame
        frame.setVisible(true);
    }

    public String getTextAreaSalida() {
        return salida.getText();
    }

    public void setTextAreaSalida(String nombre) {
        salida.setText(nombre);
    }

    public String getTextAreaEntrada() {
        return entrada.getText();
    }

    public void setTextAreaEntrada(String nombre) {
        entrada.setText(nombre);
    }

    public void addBotonCodificar(ActionListener al) {
        botonCodificar.addActionListener(al);
    }

    public void mostrarExcepcion(String s) {
        JOptionPane.showMessageDialog(frame, s, "Error!", JOptionPane.ERROR_MESSAGE);
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            mostrarExcepcion(event.getActionCommand());
        }
    }
}
