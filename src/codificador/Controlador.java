package codificador;

import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controlador {

    //Declaro referencias al modelo y a la vista
    private Modelo modelo;
    private Vista vista;

    //Declaro el constructor, donde le paso las referencias al modelo y a la vista
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
    }

    //Cuando corro el programa, este método es el primero que se ejecuta
    public void ejecutar() {
        //Llamo al método 'mostrar' de la vista.
        vista.mostrar();
        //Al boton 'avanzar' de la vista, le asocio el evento 'AvanzarListener'
        vista.addBotonCodificar(new CodificarListener());
    }


    private class CodificarListener implements ActionListener {

        @Override
        //Cuando pulso el boton 'codificar', ejecuto el siguiente bloque.
        public void actionPerformed(ActionEvent event) {
       
            try {
           
               if(modelo.codificarTexto(vista.getTextAreaEntrada()))
                vista.setTextAreaSalida(modelo.getResultadoCodificado());
                else
                    vista.setTextAreaSalida("Error: se ingresó un caracter no valido");;
               
            } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void reportException(String message) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
